<?php

namespace Nodesky\ShuntingYardAlgorithm;


use Nodesky\ShuntingYardAlgorithm\Operators\AbstractOperator;
use Nodesky\ShuntingYardAlgorithm\Operators\OperatorFactory;

class Infix2Postfix
{

    private $postfix = array();
    private $stack = array();

    public function process($infix)
    {
        $infix = (string)$infix;

        $infix = str_replace(
            [' ', '(-', ',-'],
            ['', '(0-', ',0-'],
            $infix);

        if ($infix[0] == '-') {
            $infix = '0' . $infix;
        }

        $operators = OperatorFactory::getTokens();
        $operators = array_map('preg_quote', $operators);
        $operators = '(' . implode(')|(', $operators) . ')';
        $pattern = '#(\\d+(\.?\\d+|\\d*))|(\()|(\))|' . $operators . '#';
        $tokens = array();

        preg_match_all($pattern, $infix, $tokens);

        $tokens = array_map(array(OperatorFactory::class, 'getOperator'), $tokens[0]);

        foreach ($tokens as $token) {

            if (is_numeric($token)) {
                $this->postfix[] = $token;
            } elseif ($token == '(') {
                array_unshift($this->stack, $token);
            } elseif ($token == ')') {
                $tmp = '';
                while ($tmp <> '(') {
                    if (count($this->stack) == 0) {
                        throw new Exception('Parse error.');
                    }
                    $tmp = array_shift($this->stack);
                    if ($tmp != '(') {
                        $this->postfix[] = $tmp;
                    }
                }
            } elseif ($token instanceof AbstractOperator) {

                $element = isset($this->stack[0]) ? $this->stack[0] : null;

                while ($element instanceof AbstractOperator) {
                    if ($token->comparePriority($this->stack[0]) == 1 && $this->stack[0]->getAssociative() == AbstractOperator::ASSOCIATIVE_LEFT) {
                        break;
                    }
                    if ($token->comparePriority($this->stack[0]) >= 0 && $this->stack[0]->getAssociative() == AbstractOperator::ASSOCIATIVE_RIGHT) {
                        break;
                    }
                    $this->postfix[] = array_shift($this->stack);
                }

                array_unshift($this->stack, $token);
            }
        }
        foreach ($this->stack as $token) {
            if (!($token instanceof AbstractOperator)) {
                throw new Exception('Parse error.');
            }
            $this->postfix[] = $token;
        }

        return $this->postfix;
    }
}
