<?php

namespace Nodesky\ShuntingYardAlgorithm\Operators;


abstract class OperatorFactory{

    private static $operators = array(
        '+' => 'add',
        '-' => 'sub',
        '*' => 'mul',
        '/' => 'div',
        '^' => 'pow'
    );

    public static function getTokens()
    {
        return array_keys(self::$operators);
    }

    public static function getOperator($token)
    {
        if (!array_key_exists($token, self::$operators)){
            return $token;
        }

        $namespaceOperator = __NAMESPACE__;

        $class = $namespaceOperator . '\\' . ucfirst(self::$operators[$token] . 'Operator');

        if (!class_exists($class)){
            throw new Exception('Operator class "' . $class . '" not found.');
        }

        $operator = new $class();
        if (!($operator instanceof AbstractOperator)){
            throw new Exception('Operator class "' . $class . '" must be instance of AbstractOperator and instance of ' . get_class($operator) . ' given.');
        }

        return $operator;
    }
}
