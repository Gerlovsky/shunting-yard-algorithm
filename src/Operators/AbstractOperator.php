<?php

namespace Nodesky\ShuntingYardAlgorithm\Operators;


abstract class AbstractOperator
{

    const ASSOCIATIVE_LEFT = 0;
    const ASSOCIATIVE_RIGHT = 1;
    protected $priority = null;
    protected $token = null;
    protected $operands_count = null;
    protected $associative = null;
    private $associative_map = array(
        self::ASSOCIATIVE_LEFT,
        self::ASSOCIATIVE_RIGHT
    );

    final public function __construct()
    {

    }

    public function execute(array $operands)
    {
        if (count($operands) != $this->getOperandsCount()) {
            throw new Exception('Operands count must be equal ' . $this->getOperandsCount());
        }
        $operands = array_values($operands);

        return $this->doExecute($operands);
    }

    public function getOperandsCount()
    {
        if (is_null($this->operands_count)) {
            throw new Exception('Operands count is empty');
        }

        return $this->operands_count;
    }

    abstract protected function doExecute(array $operands);

    public function getAssociative()
    {
        if (is_null($this->associative)) {
            throw new Exception('Associative is empty');
        }
        if (!in_array($this->associative, $this->associative_map)) {
            throw new Exception('Invalid associative value');
        }

        return $this->associative;
    }

    public function comparePriority(AbstractOperator $operator)
    {
        if (is_null($this->priority)) {
            throw new Exception('Priority is empty');
        }
        $num = $this->priority - $operator->priority;

        return ($num > 0) ? 1 : (($num < 0) ? -1 : 0);
    }

    public function __toString()
    {
        if (is_null($this->token)) {
            throw new Exception('Token is empty');
        }

        return $this->token;
    }
}
