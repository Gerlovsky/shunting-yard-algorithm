<?php

namespace Nodesky\ShuntingYardAlgorithm\Operators;


class PowOperator extends AbstractOperator
{
    protected $priority = 2;

    protected $token = '^';

    protected $operands_count = 2;

    protected $associative = parent::ASSOCIATIVE_RIGHT;

    protected function doExecute(array $operands)
    {
        return pow($operands[0], $operands[1]);
    }
}
