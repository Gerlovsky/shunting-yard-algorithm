<?php

namespace Nodesky\ShuntingYardAlgorithm\Operators;


class MulOperator extends AbstractOperator
{
    protected $priority = 1;

    protected $token = '*';

    protected $operands_count = 2;

    protected $associative = parent::ASSOCIATIVE_LEFT;

    protected function doExecute(array $operands)
    {
        return $operands[0] * $operands[1];
    }
}
