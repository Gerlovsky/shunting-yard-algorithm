<?php

namespace Nodesky\ShuntingYardAlgorithm\Operators;


class DivOperator extends AbstractOperator
{
    protected $priority = 1;

    protected $token = '/';

    protected $operands_count = 2;

    protected $associative = parent::ASSOCIATIVE_LEFT;

    protected function doExecute(array $operands)
    {
        if ($operands[0] == 0) {
            throw new \Exception('Division by zero');
        }

        return $operands[0] / $operands[1];
    }
}
