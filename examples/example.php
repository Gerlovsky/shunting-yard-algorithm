<?php

require_once dirname(__DIR__).'/vendor/autoload.php';

$t = new Nodesky\ShuntingYardAlgorithm\ComputeInfix();

echo $t->compute('(22.43 - 45) + 2 ^ 3');